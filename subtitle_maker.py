# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

bl_info = {
    "name": "Subtitle Maker",
    "author": "Bassam Kurdali",
    "version": (1, 0),
    "blender": (2, 78, 0),
    "location": "",
    "description": "Makes Subtitles Timed to a Script",
    "warning": "",
    "wiki_url": "",
    "category": "Animation",
    }

import bpy


handler_body = '''
import bpy
sublines = {}
times = {}
def changer(scene):
    current_frame = scene.frame_current
    body = sublines[0]
    for i, frame in enumerate(times):
        if frame >= current_frame:
            body = sublines[i]
            break
    else:
        body = sublines[-1]
    {}.body = body
bpy.app.handlers.frame_change_post.append(changer)
bpy.app.handlers.render_pre.append(changer)
'''


def find_shortest_uniques(lines, shortest=1, maxlen=3):
    shortened = []
    for line in lines:
        initial = " ".join(line.split()[:shortest])
        if initial in shortened and shortest < maxlen:
            return find_shortest_uniques(lines, shortest + 1)
        else:
            shortened.append(initial)
    return shortened
    


def split_text(text):
    """ dumb function to split script into lines, should fix"""
    sublines = []
    for line in text.split('\n'):
        if line:
            for sentence in line.split('.'):
                if sentence and not sentence.isspace():
                    sublines.append(sentence)
    return sublines


class SCENE_OT_subtitle_maker(bpy.types.Operator):
    """Make Timed Subtitles in a file"""
    bl_idname = "scene.subtitle_maker"
    bl_label = "Make timed subtitles from a text file"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return (
            context.object and context.object.data and
            context.area.type == 'TEXT_EDITOR' and context.area.spaces[0].text)

    def execute(self, context):
        script = context.area.spaces[0].text.as_string()
        scene = context.scene
        subtitles = context.object.data
        frames = scene.frame_end - scene.frame_start
        sublines = split_text(script)
        total_chars = sum(len(line) for line in sublines)  # could also get from script
        times = [0]
        for line in sublines:
            times.append(int(frames * len(line) / total_chars) + times[-1])
        times.pop(0) # don't need to change at zero
        text_body = handler_body.format(
            sublines.__repr__(),
            times.__repr__(),
            subtitles.__repr__()
            )
        print(text_body)
        local_script = bpy.data.texts.new(name="subtitle_event_changer.py")
        local_script.write(text_body)
        local_script.use_module = True
        exec(text_body)
        return {'FINISHED'}
    
    def draw(self, context):
        pass
    

class SCENE_OT_make_subtitle_markers(bpy.types.Operator):
    """Make Scene Markers for the subtitles Lines"""
    bl_idname = "scene.make_subtitle_markers"
    bl_label = "Make markers for subtitles"
    bl_options = {'REGISTER', 'UNDO'}
    
    @classmethod
    def poll(cls, context):
        return "subtitle_event_changer.py" in bpy.data.texts
    
    def execute(self, context):
        markers = context.scene.timeline_markers
        from subtitle_event_changer import sublines, times
        times = [0] + times[:-1]
        shortened = find_shortest_uniques(sublines)
        for time, line in zip(times, shortened):
            marker = markers.new(name=line)
            marker.frame = time
        return {'FINISHED'}


# Registration

def make_subtitles_button(self, context):
    self.layout.operator(
        SCENE_OT_subtitle_maker.bl_idname,
        text="Make Subtitles",
        icon='PLUGIN')

def register():
    bpy.utils.register_class(SCENE_OT_subtitle_maker)
    bpy.types.TEXT_HT_header.append(make_subtitles_button)
    bpy.utils.register_class(SCENE_OT_make_subtitle_markers)


def unregister():
    bpy.utils.unregister_class(SCENE_OT_subtitle_maker)
    bpy.types.TEXT_HT_header.remove(make_subtitles_button)
    bpy.utils.unregister_class(SCENE_OT_make_subtitle_markers)


if __name__ == "__main__":
    register()
